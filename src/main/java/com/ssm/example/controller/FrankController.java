package com.ssm.example.controller;

import com.ssm.example.annotation.Autowired;
import com.ssm.example.annotation.Controller;
import com.ssm.example.annotation.RequestMapping;
import com.ssm.example.service.FrankService;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Author: nanJunYu
 * @Description: ssm Controller 控制层
 * @Date: Create in  2018/8/13 14:38
 */
@Controller
@RequestMapping(value = "/ssm")
public class FrankController {

    @Autowired("FrankServiceImpl")
    FrankService frankService;

    @RequestMapping(value = "/test")
    public void queryParam(HttpServletResponse response) {
        try {
            PrintWriter printWriter = response.getWriter();
            String result = frankService.query("Frank", "java");
            printWriter.write(result);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
