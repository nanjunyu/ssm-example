package com.ssm.example.servlet;

import com.ssm.example.annotation.Autowired;
import com.ssm.example.annotation.Controller;
import com.ssm.example.annotation.RequestMapping;
import com.ssm.example.annotation.Service;
import com.ssm.example.controller.FrankController;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: nanJunYu
 * @Description: 所有的bean使用之前得先被创建，放到mapping里面，
 * 所以启动的时候就应该创建，在init方法里做处理
 * <p>
 * 将所有的依赖关系进行扫描建立好
 * @Date: Create in  2018/8/13 11:11
 */
public class DispatcherServlet extends HttpServlet {

    List<String> classNames = new ArrayList();

    Map<String, Object> beans = new HashMap<>();
    Map<String, Object> handlerMap = new HashMap<>();

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        this.doPost(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取请求路径
        String url = req.getRequestURI();
        String context = req.getContextPath();
        String path = url.replace(context, "");
        Method method= (Method) handlerMap.get(path);
        //根据key去拿实例
        FrankController frankController= (FrankController) beans.get("/"+path.split("/")[1]);
        try {
            method.invoke(frankController, new Object[] { req, resp});
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        } catch (InvocationTargetException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void init() throws ServletException {
        /** 第一步:把所有的bean扫描出来  扫描所有的class文件
         *  第二步:根据集合里的class类创建对象
         *  第三步:根据bean进行关系依赖处理
         *  第四步:建立完整映射关系
         */
        scanPackage("com.ssm");
        doInstance();
        doIoc();
        buildUrlMapping();
    }

    /**
     * @Author: nanJunYu
     * @Description:把所有的bean扫描出来 扫描所有的class文件
     * @Date: Create in  2018/8/13 15:10
     * @params:
     * @return:
     */
    private void scanPackage(String basePackage) {
        //拿到项目的路径开始扫描所有的class
        URL url = this.getClass().getClassLoader().getResource("/" + basePackage.replaceAll("\\.", "/"));
        String fileStr = url.getFile();
        File file = new File(fileStr);
        String fileArray[] = file.list();
        for (String path : fileArray) {
            File filePath = new File(fileStr + path);
            //代表是文件夹 继续递归
            if (filePath.isDirectory()) {
                scanPackage(basePackage + "." + path);
            } else {
                //代表是class文件目录 将完整的class物理文件路径地址 放入到集合里
                classNames.add(basePackage + "." + filePath.getName());
            }

        }
    }

    /**
     * @Author: nanJunYu
     * @Description:根据集合里的class类创建实例化对象
     * @Date: Create in  2018/8/13 15:34
     * @params:
     * @return:
     */
    private void doInstance() {
        if (classNames.isEmpty()) {
            System.out.println("class扫描失败.......");
        }
        for (String className : classNames) {
            //去除多余的后缀名
            String cName = className.replace(".class", "");
            try {
                Class<?> clazz = Class.forName(cName);
                if (clazz.isAnnotationPresent(Controller.class)) {
                    //如果是控制类 反射创建控制类
                    Object instance = clazz.newInstance();
                    RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);
                    String value = requestMapping.value();
                    //获取到@RequestMapping上配置的地址 当key放到map容器里  将实例以value的形式存放
                    beans.put(value, instance);
                } else if (clazz.isAnnotationPresent(Service.class)) {
                    Service service = clazz.getAnnotation(Service.class);
                    Object instance = clazz.newInstance();
                    //获取到@Service上配置的地址 当key放到map容器里  将实例以value的形式存放
                    beans.put(service.value(), instance);
                }
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InstantiationException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * @Author: nanJunYu
     * @Description:把Service注入到控制层
     * @Date: Create in  2018/8/13 15:53
     * @params:
     * @return:
     */
    private void doIoc() {
        if (beans.entrySet().size() <= 0) {
            System.out.println("没有一个实例类");
        }
        for (Map.Entry<String, Object> entry : beans.entrySet()) {
            Object instance = entry.getValue();
            Class<?> clazz = instance.getClass();
            if (clazz.isAnnotationPresent(Controller.class)) {
                //获得所有声明的参数 得到参数数组
                Field[] fields = clazz.getDeclaredFields();
                for (Field field : fields) {
                    if (field.isAnnotationPresent(Autowired.class)) {
                        Autowired autowired = field.getAnnotation(Autowired.class);
                        String key = autowired.value();
                        //打开私有属性的权限修改
                        field.setAccessible(true);
                        try {
                            //给变量重新设值
                            field.set(instance, beans.get(key));
                        } catch (IllegalAccessException e) {
                            e.printStackTrace();
                        }
                    } else {
                        continue;
                    }
                }
            } else {
                continue;
            }
        }
    }

    private void buildUrlMapping() {
        if (beans.entrySet().size() <= 0) {
            System.out.println("没有类的实例化......");
            return;
        }
        for (Map.Entry<String, Object> entry : beans.entrySet()) {
            Object instance = entry.getValue();
            Class<?> clazz = instance.getClass();
            if (clazz.isAnnotationPresent(Controller.class)) {
                RequestMapping requestMapping = clazz.getAnnotation(RequestMapping.class);
                String classPath = requestMapping.value();
                Method[] methods = clazz.getMethods();
                for (Method method : methods) {
                    if (method.isAnnotationPresent(RequestMapping.class)) {
                        RequestMapping methodMapping = method.getAnnotation(RequestMapping.class);
                        String methodPath = methodMapping.value();
                        //完整的映射路径
                        handlerMap.put(classPath + methodPath, method);
                    } else {
                        continue;
                    }
                }
            } else {
                continue;
            }
        }
    }
}
