package com.ssm.example.service.impl;

import com.ssm.example.annotation.Service;
import com.ssm.example.service.FrankService;

/**
 * @Author: nanJunYu
 * @Description:
 * @Date: Create in  2018/8/13 14:40
 */
@Service(value = "FrankServiceImpl")
public class FrankServiceImpl implements FrankService {
    public String query(String name, String job) {
        return "name:=" + name + "    job:=" + job;
    }
}
